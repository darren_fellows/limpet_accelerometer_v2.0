/* 
 * File:   
 * Author: 
 * Comments:
 * Revision history: 
 */

// This is a guard condition so that contents of this file are not included
// more than once.  
#ifndef SERIAL_H
#define	SERIAL_H
#endif

BYTE init_serial(void);
void start_serial(void);
