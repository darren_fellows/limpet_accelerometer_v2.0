/*
 * File:   hardware.c
 * Author: DarrenFellows
 *
 * Created on 23 August 2021, 14:43
 */

/*
 * Hardware.c and Hardware.h are the processor specific interfaces
 * All other files are intended to be agnostic of processor type for easier porting
 * Define hardware specific translations in .h and any functions in .c
 */

// PIC16F1933 Configuration Bit Settings
// 'C' source line config statements

// CONFIG1
#pragma config FOSC = INTOSC    // Oscillator Selection (INTOSC oscillator: I/O function on CLKIN pin)
#pragma config WDTE = ON        // Watchdog Timer Enable (WDT enabled)
#pragma config PWRTE = ON       // Power-up Timer Enable (PWRT enabled)
#pragma config MCLRE = ON       // MCLR Pin Function Select (MCLR/VPP pin function is MCLR)
#pragma config CP = OFF         // Flash Program Memory Code Protection (Program memory code protection is disabled)
#pragma config CPD = OFF        // Data Memory Code Protection (Data memory code protection is disabled)
#pragma config BOREN = ON       // Brown-out Reset Enable (Brown-out Reset enabled)
#pragma config CLKOUTEN = OFF   // Clock Out Enable (CLKOUT function is disabled. I/O or oscillator function on the CLKOUT pin)
#pragma config IESO = OFF       // Internal/External Switchover (Internal/External Switchover mode is disabled)
#pragma config FCMEN = OFF      // Fail-Safe Clock Monitor Enable (Fail-Safe Clock Monitor is disabled)

// CONFIG2
#pragma config WRT = OFF        // Flash Memory Self-Write Protection (Write protection off)
#pragma config VCAPEN = OFF     // Voltage Regulator Capacitor Enable (All VCAP pin functionality is disabled)
#pragma config PLLEN = OFF      // PLL Enable (4x PLL disabled)
#pragma config STVREN = ON      // Stack Overflow/Underflow Reset Enable (Stack Overflow or Underflow will cause a Reset)
#pragma config BORV = HI        // Brown-out Reset Voltage Selection (Brown-out Reset Voltage (Vbor), high trip point selected.)
#pragma config LVP = ON         // Low-Voltage Programming Enable (Low-voltage programming enabled)

#include "standard.h"
#include "hardware.h"

void init_io(void)
{
    INTERRUPT_GlobalInterrupt(DISABLE);
    INTERRUPT_PeripheralInterrupt(DISABLE);
    //configure inputs and outputs
    TRISA = 0b00000000;     //set port A all outputs
    TRISB = 0b00000000;     //set port B all outputs
    TRISC = 0b10010001;     //set port C outputs except RC0, RC4 and RC7
    TRISE = 0b00000000;     //set port E all outputs
    
    //set defaults to output pins
    PORTA = PORTA_DEFAULTS;
    PORTB = PORTB_DEFAULTS;
    PORTC = PORTC_DEFAULTS;
    PORTE = PORTE_DEFAULTS;
}

void init_sys(void)
{
    //define clock speed as 16MHZ and osc source controlled by FOC 
    OSCCONbits.SCS = FOC_OSC;
    OSCCONbits.SPLLEN = PLL_DISABLED;
    OSCCONbits.IRCF = CLOCK_16MHZ_HF;
    //set interrupts
    
}

/*
 * reset_reason returns a character relating to the reset reason
 */
char reset_reason(void)
{
    char resetReason = NULL;
    
    if (PCONbits.nPOR == 0)
    {
        //Power on reset
        resetReason = 'P';
    }
    else if (PCONbits.nBOR == 0)
    {
        //Brown out detected
        resetReason = 'B';
    }
    else if (PCONbits.nRMCLR == 0)
    {
        //MLCR pin reset
        resetReason = 'M';
    }
    else if (PCONbits.nRI == 0)
    {
        //reset instruction from software
        resetReason = 'S';
    }
    else if (PCONbits.STKUNF == 1)
    {
        //stack underflow reset
        resetReason = 'U';
    }
    else if (PCONbits.STKOVF == 1)
    {
        //stack overflow reset
        resetReason = 'O';
    }
    return resetReason;
}
