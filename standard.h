/* 
 * File:   
 * Author: 
 * Comments:
 * Revision history: 
 */

// This is a guard condition so that contents of this file are not included
// more than once.  
#ifndef STANDARD_H
#define	STANDARD_H
#endif

//standard definitions
#define BYTE unsigned char

#define FALSE 0
#define TRUE !FALSE

#define DISABLE 0
#define ENABLE 1

#define NULL 0x00

typedef enum
{
    NO_EXIT = 0,
    UNKNOWN,
    BOOTLOADER,
    INIT_ERROR,
}T_EXIT_TYPE;

struct
{
    BYTE addressLo;
    BYTE addressHi;
    BYTE qtyLo;
    BYTE qtyHi;
//    T_MODBUS_FUNCTION modbusFunction;
}T_RECEIVED_DATA_STRUCT;
