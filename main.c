/*
 * File:   main.c
 * Author: DarrenFellows
 *
 * Created on 20 August 2021, 12:28
 */

#include "standard.h"
#include "hardware.h"
#include "serial.h"
#include "spi.h"

//local prototypes
BYTE init(void);
void start_all(void);

T_EXIT_TYPE main(void)
{
    T_EXIT_TYPE exit = NO_EXIT;
    char resetReason = reset_reason();
    
    //TODO analyse and store reset reason
    
    
    //initialise everything
    BYTE error = init();
    //if ok then start systems
    if (0 == error)
    {
        start_all();
    }
    else
    {
        //exit with init error
        exit = INIT_ERROR;
    }
    
    while (exit == NO_EXIT)
    {
        //todo: write the main loop here
        
        
    }
    return exit;
}

//init is a general initialise of everything
BYTE init(void)
{
    BYTE error = 0;
    //set the IO pins up
    init_io();
    //set the system up
    init_sys();
    //set the serial port up
    error += init_serial();
    //set the spi port up
    error += init_spi();
    
    return error;
}

//start_all is a general start of services including interrupts
void start_all(void)
{
    start_serial();
    //last thing enable interrupts
    INTERRUPT_PeripheralInterrupt(ENABLE);
    INTERRUPT_GlobalInterrupt(ENABLE);
}



void __interrupt() main_isr(void)
{
    if (INTERRUPT_Timer0_Flag == TRUE)
    {
        if (INTERRUPT_Timer0_Enable == TRUE)
        {
            //Do timer related interrupt code here
        }
        INTERRUPT_Timer0_Flag = FALSE;
    }
    if (INTERRUPT_Timer0_Flag == TRUE)
    {
        if (INTERRUPT_Timer0_Enable == TRUE)
        {
            //Do timer related interrupt code here
        }
        INTERRUPT_Timer0_Flag = FALSE;
    }
}