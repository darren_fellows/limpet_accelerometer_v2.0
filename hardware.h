/* 
 * File:   
 * Author: 
 * Comments:
 * Revision history: 
 */

// This is a guard condition so that contents of this file are not included
// more than once.  
#ifndef HARDWARE_H
#define	HARDWARE_H
#endif

#include "pic16lf1933.h"

//global functions
void init_io(void);
void init_sys(void);
char reset_reason(void);

#define INTERRUPT_GlobalInterrupt(enable) (INTCONbits.GIE = enable)
#define INTERRUPT_PeripheralInterrupt(enable) (INTCONbits.PEIE = enable)

//IO pin defines

#define PORTA_DEFAULTS  0b00100010  //ensure accelerometer in reset and RS485 is set for receive
#define PORTB_DEFAULTS  0b00000000
#define PORTC_DEFAULTS  0b00000000
#define PORTE_DEFAULTS  0b00000000

//interrupt defines

#define INTERRUPT_OnChange_Flag INTCONbits.IOCIF
#define INTERRUPT_OnChange_Enable INTCONbits.IOCIE

#define INTERRUPT_Pin_Flag INTCONbits.INTF
#define INTERRUPT_Pin_Enable INTCONbits.INTE

#define INTERRUPT_Timer0_Flag INTCONbits.TMR0IF
#define INTERRUPT_Timer0_Enable INTCONbits.TMR0IE

#define INTERRUPT_Timer1_Flag PIR1bits.TMR1IF
#define INTERRUPT_Timer1_Enable PIE1bits.TMR1IE

#define INTERRUPT_Timer2_Flag PIR1bits.TMR2IF
#define INTERRUPT_Timer2_Enable PIE1bits.TMR2IE

//clock speed settings for OSCCON reg IRCF bits
#define CLOCK_16MHZ_HF 0b1111
#define CLOCK_8MHZor32MHZ_HF 0b1110
#define CLOCK_4MHZ_HF 0b1101
#define CLOCK_2MHZ_HF 0b1100
#define CLOCK_1MHZ_HF 0b1011
#define CLOCK_500KHZ_HF 0b1010
#define CLOCK_250KHZ_HF 0b1001
#define CLOCK_125KHZ_HF 0b1000
#define CLOCK_500KHZ_MF 0b0111
#define CLOCK_250KHZ_MF 0b0110
#define CLOCK_125KHZ_MF 0b0101
#define CLOCK_62_5KHZ_MF 0b0100
#define CLOCK_31_25KHZ_HF 0b0011
#define CLOCK_31_25KHZ_MF 0b0010
#define CLOCK_31KHZ_LF 0b0000
//clock source settings for OSCCON reg SCS bits
#define INTERNAL_OSC 0b11
#define TIMER1_OSC 0b01
#define FOC_OSC 0b00
//clock PLL settings for OSCCON reg SPLLEN bit
#define PLL_ENABLED 1
#define PLL_DISABLED 0